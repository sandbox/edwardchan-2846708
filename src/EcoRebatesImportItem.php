<?php

namespace Drupal\eco_rebates;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;
use Drupal\taxonomy\Entity\Term;
use Drupal\Core\Entity\Query\QueryException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Class for processing the import of an item.
 */
class EcoRebatesImportItem {

  /**
   * The item data to process.
   *
   * @var array
   */
  protected $data;

  /**
   * The field mapping.
   *
   * @var array
   */
  protected $fieldMapping;

  /**
   * The content type.
   *
   * @var string
   */
  protected $contentType;

  /**
   * Whether or not the rebate has expired.
   *
   * @var bool
   */
  protected $isExpired;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The configFactory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The field values for the current node to be imported.
   *
   * @var array
   */
  protected $nodeValues;

  /**
   * Constructs an EcoRebatesImportItem object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The query factory.
   * @param array $eco_rebates_item_data
   *   The item's data to be processed.
   * @param array $field_mapping
   *   The field mapping.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, QueryFactory $entity_query, LoggerChannelFactory $logger, EntityFieldManager $entity_field_manager, ConfigFactoryInterface $config_factory, array $eco_rebates_item_data, array $field_mapping) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityQuery = $entity_query;
    $this->logger = $logger;
    $this->entityFieldManager = $entity_field_manager;
    $this->data = $eco_rebates_item_data;
    $this->fieldMapping = $field_mapping;
    $this->config = $config_factory;
  }

  /**
   * Performs the processing for the import.
   */
  public function import() {
    $field_mapping = $this->fieldMapping;
    $item_data = $this->data;

    // Get the entity type and content type.
    $content_type = $field_mapping['bundle'];

    $product = $item_data['product'];
    $brand = isset($product['brand']) ? $product['brand'] : '';
    $sku = isset($product['sku']) ? $product['sku'] : '';
    $title = $brand . ' ' . $sku . ' Rebate';

    $fields = [];
    // Check to see if a parent key was specified for the entire response.
    // If so, access that element in the data array.
    if (isset($field_mapping['iterate']) && $mapping = $field_mapping['iterate']) {
      if (isset($mapping['parent'])) {
        $parent_key = $mapping['parent'];
        $item_data = $item_data[$parent_key];
      }
      if (isset($mapping['fields'])) {
        $fields = $mapping['fields'];
      }
    }

    // Create the initial node values.
    $this->nodeValues = [
      'title' => $title,
      'type' => $content_type,
      // If the rebate has expired, create as unpublished.
      'status' => !$this->isExpired,
    ];
    // Iterate through the field mapping to process the values.
    foreach ($fields as $field) {
      // The response item's data will be used to narrow down to the level
      // of the 'parent' keys as defined in the mapping configuration.
      $field_data = $item_data;

      // Check to see if a parent was specified for the field.
      // If so, access that element in the data array.
      if (isset($field['parent']) && isset($item_data[$field['parent']])) {
        $field_data = $item_data[$field['parent']];
      }

      // Call the helper method to generate the correct field format based
      // on the field type.
      $options = $this->getFieldOptions($field, $field_data, $this->data);

      // Using the returned field value option, add it to the node_values array.
      if ($options) {
        // Append it to the node_values array to be set on the nodes later.
        $this->nodeValues[$field['drupal_key']] = $options;
      }
    }

    return $this->save();
  }

  /**
   * Saves the node.
   *
   * @return bool|int
   *   Returns the node id or FALSE if save was unsuccessful.
   */
  public function save() {
    // Return false if the entity already exists.
    if ($this->entityExists()) {
      return FALSE;
    }
    // Create and save the node.
    try {
      // Create the node and pass the field values array.
      $node = Node::create($this->nodeValues);
      $node->save();
    }
    // Throw an exception if node save was unsuccessful.
    catch (EntityStorageException $e) {
      $this->logger->get('eco_rebates')->error($e->getMessage());
      return FALSE;
    }

    return $node->id();
  }

  /**
   * Returns the field type based on the field name.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return string
   *   The field type.
   */
  public function getFieldType($field_name) {
    $field_mapping = $this->fieldMapping;
    $content_type = $field_mapping['bundle'];

    $field_definitions = $this->entityFieldManager->getFieldDefinitions('node', $content_type);
    $field_definition = $field_definitions[$field_name];
    $field_storage_definition = $field_definition->getFieldStorageDefinition();
    $field_type = $field_storage_definition->getType();

    return $field_type;
  }

  /**
   * Get the field options.
   */
  public function getFieldOptions($field, $response, $item_data = []) {
    // If ther is no drupal_key defined in the mapping, return false.
    if (!isset($field['drupal_key'])) {
      return FALSE;
    }

    $output = NULL;
    // Get the field type for the field.
    $field_type = $this->getFieldType($field['drupal_key']);
    // Format the field value output for each type.
    switch ($field_type) {
      case 'daterange':
        $output = $this->formatDateRange($field, $response);
        break;

      case 'text_long':
        $field_name = $field['response_key'];
        $text_long = $response[$field_name];
        $output = [
          'format' => 'basic_html',
          'value' => implode("<br>", $text_long),
        ];
        break;

      case 'entity_reference':
        $output = $this->getEntityReference($field, $response, $item_data);
        break;

      default:
        $field_name = $field['response_key'];
        if (isset($response[$field_name])) {
          $output = [$response[$field_name]];
        }
        break;
    }

    return $output;
  }

  /**
   * Helper function to return the field value format based on entity type.
   */
  protected function getEntityReference($field, $response, $item_data = []) {
    // An 'entity_type' must be specified in the configuration.
    if (!isset($field['entity_type']) || !$field['entity_type']) {
      throw new \Exception("'entity_type' missing in mapping configuration.");
    }

    // The field value output format to be stored on the field.
    $output = NULL;

    // Process the references by calling helper functions based on entity type.
    switch ($field['entity_type']) {
      case 'node':
        $output = $this->processNodeReference($field, $response, $item_data);
        break;

      case 'taxonomy_term':
        $output = $this->processTaxonomyTermReference($field, $response, $item_data);
        break;

      default:
        break;
    }

    return $output;
  }

  /**
   * Helper function to get the node reference.
   */
  protected function processNodeReference($field, $response, $item_data = []) {
    // The 'bundle' is required for the 'node' reference field.
    if (!isset($field['bundle'])) {
      throw new \Exception("'bundle' missing for 'node' reference field.");
    }

    // The conditions to pass into the entity query.
    $args = ['type' => $field['bundle']];

    switch ($field['bundle']) {
      // For model references, use the product's UPC value to find a 'model'
      // node, which is the salsify_id.
      case 'model':
        $id = $this->getProductUpc($item_data);
        $args += ['salsify_salsifyid' => $id];
        break;

      default:
        break;
    }

    try {
      // Create the query with the condition args to get the node reference.
      $query = $this->entityQuery->get('node');
      foreach ($args as $name => $value) {
        $query->condition($name, $value);
      }
      $nids = $query
        ->range(0, 1)
        ->execute();
      $nid = reset($nids);
    }
    catch (QueryException $exception) {
      $this->logger->get('eco_rebates')
        ->notice('Could not execute the query because of error "%error".', ['%error' => $exception->getMessage()]);
    }

    return !empty($nid) ? ['target_id' => $nid] : NULL;
  }

  /**
   * Returns the product UPC, which is used to reference 'model' nodes.
   */
  protected function getProductUpc($item_data) {
    if (!isset($item_data['product'])) {
      throw new \Exception("There is no 'product' value returned from the response.");
    }

    return isset($item_data['product']['upc']) ? $item_data['product']['upc'] : NULL;
  }

  /**
   * Returns the taxonomy tid field output and creates one if it does not exist.
   */
  protected function processTaxonomyTermReference($field, $response) {
    // Require that 'vid' exists if the type is 'taxonomy_term'.
    if (!isset($field['vid'])) {
      throw new \Exception("'vid' missing for 'taxonomy_term' reference field.");
    }

    $vid = $field['vid'];
    // Set the initial property as the value of the taxonomy vid.
    $properties = ['vid' => $vid];
    // The name of the taxonomy term to search.
    $name = NULL;

    // Retrieve the name of the term to look up based on the vocabulary.
    switch ($vid) {
      // For 'rebate_source' terms, retrieve from the static method.
      case 'rebate_source':
        $name = EcoRebatesImport::getRebateSource();
        break;

      default:
        break;
    }

    // Return null if no 'name' is provided for the lookup.
    if (!$name) {
      return NULL;
    }

    // Set the 'name' property to the value to look up.
    $properties['name'] = $name;

    // Check if taxonomy term already exists with the name.
    $terms = $this->entityTypeManager
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);
    $term = reset($terms);

    // If the term does not currently exist, create that term.
    if (empty($term)) {
      $term = Term::create([
        'vid' => $field['vid'],
        'name' => $name,
      ]);
      $term->save();
    }

    // Return the term id.
    return $term->id();
  }

  /**
   * Format the date range values.
   */
  protected function formatDateRange($field, $response) {
    // Require that 'response_key' exists if the type is 'daterange'.
    if (!isset($field['response_key'])) {
      throw new \Exception("'response_key' missing for 'daterange' field.");
    }

    // Get the configuration to check the 'set_expired_unpublished' value.
    $config = $this->config->get('eco_rebates.settings');
    $unpublish_expired = $config->get('set_expired_unpublished');

    $response_key = $field['response_key'];

    // Convert all non-array values into an array.
    if (!is_array($response_key)) {
      $response_key = [] + ['value' => $response_key];
    }

    // The array to hold the field values to store in the field.
    $output = [];

    // Get the date format in the response to be converted to a format that
    // can be stored in Drupal.
    $from_format = isset($field['process']['from_format']) ? $field['process']['from_format'] : 'F j, Y';

    // The format that dates should be stored in ('Y-m-d').
    $storage_format = DATETIME_DATE_STORAGE_FORMAT;

    // Iterate through each key and convert the dates to date storage format, in
    // which will be stored in Drupal.
    foreach ($response_key as $key => $value) {
      // If the key doesn't exist in the response, continue.
      if (!isset($response[$value])) {
        continue;
      }

      // Get the date value and format it.
      $date_value = $response[$value];
      $formatted = DrupalDateTime::createFromFormat($from_format, $date_value)
        ->format($storage_format);

      // Add this key and value to the output array.
      $output += [$key => $formatted];

      // Check for the end_value date and if it is older than the current
      // date, set the isExpired flag to TRUE. This will be used to determine
      // whether a 'rebate' node is published or unpublished by default.
      if ($key == 'end_value') {
        // Get the current date.
        $current_date = DrupalDateTime::createFromTimestamp(time())->format($storage_format);
        // If the response date is older than today and the config
        // form has the 'Set expired rebate nodes to unpublished' checked,
        // set the isExpired property to TRUE.
        if (($formatted < $current_date) && $unpublish_expired) {
          $this->isExpired = TRUE;
        }
      }
    }

    return $output;
  }

  /**
   * Checks if the node entity already exists with the provided properties.
   *
   * @return bool
   *   Whether or not the node entity currently exists.
   */
  protected function entityExists() {
    // Load the properties and see if anything is turned.
    $results = $this->entityTypeManager
      ->getStorage('node')
      ->loadByProperties($this->nodeValues);
    // If something is returned, the entity exists - otherwise, return false.
    return !empty($results);
  }

}
