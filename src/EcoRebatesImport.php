<?php

namespace Drupal\eco_rebates;

/**
 * Extends the methods in EcoRebatesImportBase to process the imports.
 *
 * The sub-class extending EcoRebatesImportBase used to perform content imports
 * from data retrieved from the Eco Rebates API.
 *
 * @package Drupal\eco_rebates
 */
class EcoRebatesImport extends EcoRebatesImportBase {

  /**
   * {@inheritdoc}
   */
  public function processEcoRebatesItem(array $eco_rebates_item_data, array $field_mapping) {
    // Check for 0 indexes in the array - if it exists, move it up one level.
    array_walk($eco_rebates_item_data, [$this, 'cleanUpDataArray']);
    // Create the import item object and run the import.
    $item = new EcoRebatesImportItem($this->entityTypeManager(), $this->entityQuery, $this->logger, $this->entityFieldManager, $this->configFactory, $eco_rebates_item_data, $field_mapping);
    return $item->import();
  }

  /**
   * {@inheritdoc}
   */
  public function processEcoRebatesData($response, array $field_mapping) {
    $response = json_decode($response, TRUE);

    if (isset($field_mapping['parent_key'])) {
      $parent_key = $field_mapping['parent_key'];
      $response = $response[$parent_key];
    }

    // Make sure the key containing the rebate objects exists.
    $data_item = isset($response['productRebateDetails']) ? $response['productRebateDetails'] : [];

    // Keep track of imported and failed node count.
    $success_count = $fail_count = 0;
    // Iterate through each rebate item.
    foreach ($data_item as $rebate) {
      $item = $this->processEcoRebatesItem($rebate, $field_mapping);
      if (!$item) {
        $fail_count++;
        continue;
      }
      $success_count++;
    }

    // The result showing how many nodes were imported.
    $result = [
      'imported' => $success_count,
      'failed' => $fail_count,
    ];

    return $result;
  }

  /**
   * {@inheritdoc}
   */
  public function getMappingConfigName() {
    return 'eco_rebates.mapping';
  }

  /**
   * {@inheritdoc}
   */
  public static function getRebateSource() {
    return 'Eco Rebates';
  }

}
