<?php

namespace Drupal\eco_rebates;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\QueryFactory;
use Drupal\Core\Logger\LoggerChannelFactory;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Config\ConfigValueException;
use Drupal\Core\Entity\EntityFieldManager;

/**
 * Provides methods for Eco Rebates API integration and node imports.
 *
 * The base class that interacts with the Eco Rebates API and provides abstract
 * methods for performing content imports. Imports are triggered either
 * through queues during a cron run or via the configuration page.
 *
 * @package Drupal\eco_rebates
 */
abstract class EcoRebatesImportBase {

  /**
   * The configFactory interface.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The Eco Rebates config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Entity query factory.
   *
   * @var \Drupal\Core\Entity\Query\QueryFactory
   */
  protected $entityQuery;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * Returns the rebate source.
   */
  abstract public static function getRebateSource();

  /**
   * Returns the mapping configuration file name.
   */
  abstract public function getMappingConfigName();

  /**
   * Processes the Eco Rebates data response.
   *
   * @param string $response
   *   The JSON response string returned from the endpoint.
   * @param array $eco_rebates_data
   *   The Eco Rebates response item to process.
   */
  abstract public function processEcoRebatesData($response, array $eco_rebates_data);

  /**
   * A function to import Eco Rebates data as nodes in Drupal.
   *
   * @param array $eco_rebates_item_data
   *   The Eco Rebates individual data to process.
   * @param array $field_mapping
   *   The field mapping of the current import.
   */
  abstract public function processEcoRebatesItem(array $eco_rebates_item_data, array $field_mapping);

  /**
   * Constructs a \Drupal\eco_rebates\EcoRebatesImport object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory interface.
   * @param \Drupal\Core\Entity\Query\QueryFactory $entity_query
   *   The query factory.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    QueryFactory $entity_query,
    EntityTypeManagerInterface $entity_type_manager,
    LoggerChannelFactory $logger,
    ClientInterface $http_client,
    EntityFieldManager $entity_field_manager) {
    $this->configFactory = $config_factory;
    $this->config = $this->configFactory->get('eco_rebates.settings');
    $this->entityQuery = $entity_query;
    $this->entityTypeManager = $entity_type_manager;
    $this->logger = $logger;
    $this->httpClient = $http_client;
    $this->entityFieldManager = $entity_field_manager;
  }

  /**
   * Starts the ingestion process.
   */
  public function startImport() {
    if (!$this->getMapping()) {
      throw new \Exception('Could not find or validate the field mapping.');
    }

    $endpoints = $this->getEndpoints();
    // If there are no endpoints, return an error.
    if (!$endpoints) {
      return [
        'status' => 'error',
        'message' => t('No endpoints were specified.'),
      ];
    }

    $message = '';
    // Iterate through each endpoint and process the data.
    foreach ($endpoints as $url) {
      // If no response is returned from the url, throw an exception.
      if (!$this->getResponse($url)) {
        throw new \Exception('Could not get a response from the Eco Rebates API.');
      }
      // Process the response data and import nodes.
      $result = $this->processEcoRebatesData($this->getResponse($url), $this->getMapping());
      $message = 'The Eco Rebates import is complete: ';
      foreach ($result as $status => $count) {
        $message .= " $count nodes $status.";
      }
    }

    return [
      'status' => 'status',
      'message' => $message,
    ];
  }

  /**
   * Returns the endpoints from the configuration.
   */
  public function getEndpoints() {
    return array_values($this->config->get('endpoint_urls'));
  }

  /**
   * Gets the mapping array from the configuration file.
   */
  public function getMapping() {
    $mapping_filename = $this->getMappingConfigName();
    $config = $this->config($mapping_filename)->get();
    return $this->configValidate($config);
  }

  /**
   * Gets the response from the endpoint.
   */
  public function getResponse($url) {
    $url = $this->processUrlPlaceholders($url);

    try {
      $data = (string) $this->httpClient
        ->get($url, ['headers' => ['Accept' => 'application/json']])
        ->getBody();

      return $data;
    }
    catch (RequestException $exception) {
      $this->logger->get('eco_rebates')->notice('Could not make GET request to %endpoint because of error "%error".', ['%endpoint' => $url, '%error' => $exception->getMessage()]);
      return FALSE;
    }
  }

  /**
   * Checks the urls for placeholders and replaces them.
   *
   * @param string $url
   *   The url to check for placeholders.
   *
   * @return string
   *   The url with the placeholder values.
   */
  public function processUrlPlaceholders($url) {
    $placeholder_tokens = $this->config->get('placeholder_tokens');
    foreach ($placeholder_tokens as $key => $placeholder) {
      // If the placeholder exists, replace the placeholders with the values
      // defined in the config.
      if (strpos($url, $placeholder) && $config_value = $this->config->get($key)) {
        $url = str_replace($placeholder, $config_value, $url);
      }
    }

    return $url;
  }

  /**
   * Validates the configuration file to make sure all required fields exist.
   */
  protected function configValidate($response) {
    // The keys of the required properties in the configuration.
    $required = $this->config->get('required_fields');
    try {
      foreach ($required as $name) {
        if (!isset($response[$name])) {
          throw new ConfigValueException($name . ' must be specified in the configuration.');
        }
      }
    }
    catch (ConfigValueException $e) {
      $this->logger->get('eco_rebates')->notice('Could not process data because of error "%error".', ['%error' => $e->getMessage()]);
      return FALSE;
    }

    return $response;
  }

  /**
   * Helper function to check and shift arrays with 0 indices.
   *
   * @param mixed $item
   *   The item to check.
   * @param string $key
   *   The key of the array.
   */
  protected function cleanUpDataArray(&$item, $key) {
    // Shift the associative arrays to remove the indices.
    if (is_array($item) && array_key_exists(0, $item)) {
      $item = array_shift($item);
    }
  }

  /**
   * Returns the Eco Rebates site id saved in the configuration form.
   *
   * @return string
   *   The Eco Rebates site id.
   */
  protected function getSiteId() {
    return $this->config->get('eco_rebates_id');
  }

  /**
   * Retrieves the entity type manager.
   *
   * @return \Drupal\Core\Entity\EntityTypeManagerInterface
   *   The entity type manager.
   */
  protected function entityTypeManager() {
    if (!isset($this->entityTypeManager)) {
      $this->entityTypeManager = $this->container()->get('entity_type.manager');
    }
    return $this->entityTypeManager;
  }

  /**
   * Retrieves the entity query object for an entity type.
   *
   * @param string $entity_type
   *   The type of the entity to query.
   *
   * @return \Drupal\Core\Entity\Query\QueryInterface
   *   The query object that can query the given entity type.
   */
  protected function entityQuery($entity_type) {
    if (!$this->entityQuery) {
      $this->entityQuery = $this->container()->get('entity.query');
    }
    return $this->entityQuery->get($name);
  }

  /**
   * Retrieves a configuration object.
   *
   * This is the main entry point to the configuration API.
   *
   * @param string $name
   *   The name of the configuration object to retrieve.
   *
   * @return \Drupal\Core\Config\Config
   *   A configuration object.
   */
  protected function config($name) {
    if (!$this->configFactory) {
      $this->configFactory = $this->container()->get('config.factory');
    }
    return $this->configFactory->get($name);
  }

  /**
   * Returns the service container.
   *
   * This method is marked private to prevent sub-classes from retrieving
   * services from the container through it. Instead,
   * \Drupal\Core\DependencyInjection\ContainerInjectionInterface should be used
   * for injecting services.
   *
   * @return \Symfony\Component\DependencyInjection\ContainerInterface
   *   The service container.
   */
  private function container() {
    return \Drupal::getContainer();
  }

}
