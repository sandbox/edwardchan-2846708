<?php

namespace Drupal\eco_rebates\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\eco_rebates\EcoRebatesImport;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Distribution Configuration form class.
 */
class ConfigForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * The EcoRebates importer.
   *
   * @var \Drupal\eco_rebates\EcoRebatesImport
   */
  protected $importer;

  protected $bundles;

  /**
   * The entity manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, EcoRebatesImport $eco_rebates_importer) {
    $this->entityTypeManager = $entity_type_manager;
    $this->importer = $eco_rebates_importer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('eco_rebates.importer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'eco_rebates_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('eco_rebates.settings');

    $form['eco_rebates_api_settings'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Eco Rebates API Settings'),
      '#collapsible' => TRUE,
      '#group' => 'eco_rebates_api_settings_group',
    ];

    $form['eco_rebates_api_settings']['eco_rebates_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Site ID'),
      '#default_value' => $config->get('eco_rebates_id'),
      '#description' => $this->t('The site ID for your Eco Rebates account.'),
      '#required' => TRUE,
    ];

    $form['endpoints'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Endpoints'),
      '#group' => 'eco_rebates_endpoints_group',
    ];

    $endpoint_urls_text = '';
    if ($config->get('endpoint_urls')) {
      $endpoint_urls_text = implode("\n", array_values($config->get('endpoint_urls')));
    }

    $form['endpoints']['urls'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Endpoints'),
      '#description' => $this->t('The Eco Rebates API endpoints to sync with. Enter one on each line.'),
      '#default_value' => $endpoint_urls_text,
    ];

    $tokens = $config->get('placeholder_tokens');

    $form['endpoints']['placeholder_tokens_help'] = [
      '#markup' => '<p>' . $this->t('The following tokens are available.') . '</p>',
    ];

    $items = [];
    if (!empty($tokens)) {
      foreach ($tokens as $key => $value) {
        $items[] = $value;
      }
      $form['endpoints']['placeholder_tokens'] = [
        '#theme' => 'item_list',
        '#items' => $items,
      ];
    }

    // Show operations fieldset if the user has permission and site id exists.
    if ($this->currentUser()->hasPermission('access to sync operations with eco rebates') && $config->get('eco_rebates_id')) {
      $form['eco_rebates_operations'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Operations'),
        '#collapsible' => TRUE,
        '#group' => 'eco_rebates_operations_group',
      ];
      $form['eco_rebates_operations']['content_type'] = [
        '#type' => 'select',
        '#title' => $this->t('Content Type'),
        '#options' => $this->getBundles(),
        '#default_value' => $config->get('content_type'),
        '#description' => $this->t('The content type to sync the values for.'),
        '#required' => TRUE,
      ];
      $form['eco_rebates_operations']['set_expired_unpublished'] = [
        '#type' => 'checkbox',
        '#title' => 'Set expired rebate nodes to unpublished.',
        '#default_value' => $config->get('set_expired_unpublished'),
        '#ajax' => [
          'callback' => [$this, 'ajaxUnpublishExpired'],
          'event' => 'change',
        ],
      ];
      $form['eco_rebates_operations']['eco_rebates_start_import'] = [
        '#type' => 'button',
        '#value' => $this->t('Import from Eco Rebates'),
        '#prefix' => '<p>',
        '#suffix' => '</p>',
      ];
    }

    return $form;
  }

  /**
   * Ajax callback for saving the 'set_expired_unpublished' checkbox.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function ajaxUnpublishExpired(array $form, FormStateInterface $form_state) {
    $config = $this->config('eco_rebates.settings');
    $config->set('set_expired_unpublished', (int) $form_state->getValue('set_expired_unpublished'));
    $config->save();

    return $form['eco_rebates_operations']['set_expired_unpublished'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $trigger = $form_state->getTriggeringElement();
    // If the import button is the triggering element, perform the import.
    if ($trigger['#id'] == 'edit-eco-rebates-start-import') {
      // Start the import.
      $results = $this->importer->startImport();
      if ($results) {
        drupal_set_message($results['message'], $results['status']);
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('eco_rebates.settings');
    $config->set('eco_rebates_id', $form_state->getValue('eco_rebates_id'));

    $endpoint_urls = [];
    if ($form_state->getValue('urls')) {
      $textarea_lines = explode("\n", $form_state->getValue('urls'));
      foreach ($textarea_lines as $line) {
        $endpoint_urls[] = trim($line);
      }
    }
    $config->set('endpoint_urls', $endpoint_urls);
    $config->set('content_type', $form_state->getValue('content_type'));
    $config->set('set_expired_unpublished', $form_state->getValue('set_expired_unpublished'));

    // Save the configuration.
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns all available node types.
   *
   * @return array
   *   The array of all available node types.
   */
  public function getBundles() {
    if (!$this->bundles) {
      $nodeTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
      foreach ($nodeTypes as $nodeType) {
        $this->bundles[$nodeType->id()] = $nodeType->label();
      }
    }
    return $this->bundles;
  }

  /**
   * Return the configuration names.
   */
  protected function getEditableConfigNames() {
    return [
      'eco_rebates.settings',
    ];
  }

}
